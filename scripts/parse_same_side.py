#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 11:23:29 2019

@author: Samuele Garda
"""

import logging
import argparse
import random
import csv
import pandas as pd
from collections import Counter
import smart_open 
from gensim.utils import to_utf8,simple_preprocess
from gensim.parsing import preprocessing as pp
from io_utils import IOManager as iom


logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')


FILTERS = [lambda x : x.lower(), pp.strip_tags, pp.strip_punctuation, 
           pp.strip_multiple_whitespaces,pp.strip_numeric]

def parse_args():
  
  parser = argparse.ArgumentParser(description='Create pre-tokenized train and test split')
  parser.add_argument('-i','--in-dir', required=True, type = str, help='Path to csv to be processed')
  parser.add_argument('-c','--chunk', default = 1000, type = int, help='Size of chunks for reading data')
  parser.add_argument('-o','--out', required=True, type = str, help='Dir where to store parsed data')
  
  
  return parser.parse_args()

def simplify_topic(topic):
  
  if topic.find('gay marriage') > -1:
    topic = "gay"
  elif topic.find('abortion') > -1:
    topic = "abortion"
  else:
    topic = "unknown"
  
  return topic

def get_reader(path,chunksize,train):
  
  if train:
    
    reader  = pd.read_csv(path, quotechar='"',quoting=csv.QUOTE_ALL,encoding='utf-8',escapechar='\\',doublequote=False, chunksize = chunksize)
        
  else:
    
    reader = pd.read_csv(path, chunksize = chunksize)
  
  return reader


def get_dev_set_within_topic(path,chunksize):

  dev_counts = {"abortion_same" : 2000,
                "abortion_diff" : 2000,
                "gay_marriage_same" : 1300,
                "gay_marriage_diff" : 700}
  
  all_ids = {"abortion_same" : [],
             "abortion_diff" : [],
             "gay_marriage_same" : [],
             "gay_marriage_diff" : []}
  
  
  reader = get_reader(path,chunksize,train = True)
  
  for idx,chunk in enumerate(reader):
    
    chunk["topic"] = chunk["topic"].apply(simplify_topic)
  
    all_ids["abortion_same"] += chunk.loc[(chunk["topic"] == "abortion") & (chunk["is_same_side"] == True)]["id"].tolist()
    all_ids["abortion_diff"] += chunk.loc[(chunk["topic"] == "abortion") & (chunk["is_same_side"] == False)]["id"].tolist()
    all_ids["gay_marriage_same"] += chunk.loc[(chunk["topic"] == "gay") & (chunk["is_same_side"] == True)]["id"].tolist()
    all_ids["gay_marriage_diff"] += chunk.loc[(chunk["topic"] == "gay") & (chunk["is_same_side"] == False)]["id"].tolist()
    
    if (idx%10)==0:
      logger.info("Read {} instances".format(idx*chunksize))
  
  dev_ids = []
  for k,v in all_ids.items():
    random.Random(42).shuffle(v)
    dev_ids += random.Random(42).sample(all_ids.get(k),dev_counts[k])
  
  logger.info("Generated dev split for within topic task by id of size : {}".format(len(dev_ids))) 
  
  return dev_ids



def get_dev_set_cross_topic(path,chunksize):
  
  dev_counts = {"same_side" : 3000,
                "diff_side" : 3000}
  
  all_ids = {"same_side" : [],
             "diff_side" : []}
  
  reader = get_reader(path,chunksize,train = True)
  
  for idx,chunk in enumerate(reader):
    
    all_ids["same_side"] += chunk.loc[chunk["is_same_side"] == True]["id"].tolist()
    all_ids["diff_side"] += chunk.loc[chunk["is_same_side"] == False]["id"].tolist()
    
    if (idx%10)==0:
      logger.info("Read {} instances".format(idx*chunksize))
  
  dev_ids = []
  for k,v in all_ids.items():
    random.Random(42).shuffle(v)
    dev_ids += random.Random(42).sample(all_ids.get(k),dev_counts[k])
  
  logger.info("Generated dev split for cross topic task by id of size : {}".format(len(dev_ids))) 
  
  return dev_ids

def update_freq(arg_words,freqs = None):
  
  if freqs is not None:
    
    for word in arg_words:
      
      freqs[word] += 1
  
    

def get_instance_byte_update_word_freq(row,freqs = None,train = False):
  
  arg1_words = simple_preprocess(row["argument1"]) #pp.preprocess_string(row["argument1"], filters = FILTERS)
  arg2_words = simple_preprocess(row["argument2"]) #pp.preprocess_string(row["argument2"], filters = FILTERS)
  
  update_freq(arg1_words,freqs)
  update_freq(arg2_words,freqs)
  
  arg1 = ' '.join(arg1_words)
  arg2 = ' '.join(arg2_words)
  
  if train:
    
    is_same_side = str(int(row["is_same_side"]))
  
    line = to_utf8("{}\t{}\t{}\n".format(arg1,arg2,is_same_side))
  
  else:
    
    line = to_utf8("{}\t{}\n".format(arg1,arg2))
  
  return line


def write_train_dev_cmpr(path,chunksize,out_dir,dev_ids = None):
  
  logger.info("Processing {}".format(path))
  
  reader = get_reader(path,chunksize,train = True)
  
  freqs = Counter()
  
  train_file =  smart_open.open(iom.join_paths([out_dir,"train.txt.gz"]),'wb')
  dev_file = smart_open.open(iom.join_paths([out_dir,"dev.txt.gz"]),'wb')
  freqs_path = iom.join_paths([out_dir,"freqs.pkl"])
  
  for idx,chunk in enumerate(reader):
    for row_idx,row in chunk.iterrows():
      line = get_instance_byte_update_word_freq(row,freqs,train = True)
      if row['id'] in dev_ids:
        dev_file.write(line)
      else:
        train_file.write(line)
    
    if (idx%10)==0:
      logger.info("Parsed {} instances".format(idx*chunksize))
  
  iom.save_pickle(freqs,freqs_path)
  logger.info("Saved vocabulary frequencies of size at : `{}` of size {}".format(freqs_path,len(freqs)))
  train_file.close()
  dev_file.close()
  logger.info("Saved parsed training and dev data at : `{}`".format(out_dir))
  
  
def write_test_cmpr(path,chunksize,out_dir):
  
  reader = get_reader(path,chunksize,train = False)
    
  test_file =  smart_open.open(iom.join_paths([out_dir,"test.txt.gz"]),'wb')
  
  for idx,chunk in enumerate(reader):
    for row_idx,row in chunk.iterrows():
      line = get_instance_byte_update_word_freq(row)
      test_file.write(line)
    
    if (idx%10)==0:
      logger.info("Parsed {} instances".format(idx*chunksize))
  
  
  test_file.close()
    
if __name__ == "__main__":
  
  args = parse_args()
  
  in_dir = args.in_dir
  chunksize = args.chunk
  out_dir = args.out
      
  wt_out = iom.join_paths([out_dir,"wt"])
  ct_out = iom.join_paths([out_dir,"ct"])
  
  iom.make_dir(wt_out)
  iom.make_dir(ct_out)
  
  train_wt = iom.join_paths([in_dir,"within-topic","training.csv"])
  test_wt = iom.join_paths([in_dir,"within-topic","test.csv"])
  
  train_ct = iom.join_paths([in_dir,"cross-topic","training.csv"])
  test_ct = iom.join_paths([in_dir,"cross-topic","test.csv"])

  logger.info("Generating training and dev data for wt task")
    
  dev_ids = get_dev_set_within_topic(path = train_wt,chunksize = chunksize)
    
  write_train_dev_cmpr(path = train_wt,
                       chunksize = chunksize,
                       dev_ids = dev_ids,
                       out_dir = wt_out)
  
  logger.info("Generating test data for wt task")
  
  write_test_cmpr(path = test_wt,
                  chunksize = chunksize,
                  out_dir = wt_out)
  
 
  logger.info("Generating training and dev data for ct task")
    
  dev_ids = get_dev_set_cross_topic(path = train_ct,chunksize = chunksize)
    
  write_train_dev_cmpr(path = train_ct,
                       chunksize = chunksize,
                       dev_ids = dev_ids,
                       out_dir = ct_out)
  
  logger.info("Generating test data for ct task")
   
  write_test_cmpr(path = test_ct,
                  chunksize = chunksize,
                  out_dir = ct_out)
    
    
 
    
    
      
      
      

            
      
  
  
 
  
  
  
  
  

  
  
  
  
      
  
  
  
      

  
  
  
  
