#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 15:53:35 2019

@author: Samuele Garda
"""

import logging
import argparse
import itertools
from tfrecord_utils import TFRecordWriter
from io_utils import IOManager as iom
from io_utils import ConfigTemplate


logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')

def parse_args():
  
  parser = argparse.ArgumentParser(description='Create train, dev and test tfrecords files')
  parser.add_argument('-i','--in-dir', required=True, type = str, help='Dir where parsed corpus is stored')
  parser.add_argument('-t','--task', choices = ('same_side','nli'), required=True, type = str, help ='Dataset to be parsed: `same_side` or transfer nli  `nli`')
  parser.add_argument('-m','--max-freq', default = None, type = int, help='Drop words that are less frequent than this')
  parser.add_argument('-c','--config-dir', required=True, type = str, help='Dir for configuration files. Used to save number of examples.')
  parser.add_argument('-o','--out', required=True, type = str, help='Dir where to store tfrecords')
  
  return parser.parse_args()

def get_w2i(w2freqs,max_freq):
  
  vocab = w2freqs.most_common(max_freq)
  
  if max_freq:
    logger.info("Using {} most common words : {}...".format(max_freq,vocab[:10]))
    
  w2i = {k : i for i,(k,v) in enumerate(vocab,start = 2)}
  w2i["<S>"] = 1
  
  logger.info("Resulting vocabulary size : {}".format(len(w2i)))
  
  return w2i
  
  

if __name__ == "__main__":
    
  args = parse_args()
  
  in_dir = args.in_dir
  max_freq = args.max_freq
  task = args.task
  out_dir = args.out
  config_dir = args.config_dir
  
  iom.make_dir(config_dir)
  
  conf_templ = ConfigTemplate()
  
  if args.task == "same_side":
    
    logger.info("Start converting parsed Same Side dataset to tfrecords")
    
    iom.make_dir(iom.join_paths([out_dir,"wt"]))
    iom.make_dir(iom.join_paths([out_dir,"ct"]))
    
    in_txt = iom.join_paths([in_dir,"{}","{}.txt.gz"])
    out_tf = iom.join_paths([out_dir,"{}","{}.tfrecords"])
    freqs = iom.join_paths([in_dir,"{}","freqs.pkl"])
    
    wt_w2i = get_w2i(iom.load_pickle(freqs.format("wt")),max_freq)
    ct_w2i = get_w2i(iom.load_pickle(freqs.format("ct")),max_freq)
    
    w2i_path = iom.join_paths([out_dir,"{}","w2i.pkl"])
   
    tfrw = TFRecordWriter()
    
    split2n_examples = {}
    
    for task,split in itertools.product(["wt","ct"],["train","dev","test"]):
      
      logger.info("Generating tfrecords files for: task `{}` - split `{}`".format(task,split))
      
      if split == "train" or split == "dev":
      
        n_ex = tfrw.write_labeled_split(in_file = in_txt.format(task,split),
                                        out_file = out_tf.format(task,split),
                                        w2i = wt_w2i if task == "wt" else ct_w2i)
      
        split2n_examples["{}_{}".format(task,split)] = n_ex
        
        
      else:
        
        n_ex = tfrw.write_test_split(in_file = in_txt.format(task,split),
                                     out_file = out_tf.format(task,split),
                                     w2i =  wt_w2i if task == "wt" else ct_w2i)
      
        split2n_examples["{}_{}".format(task,split)] = n_ex
        
  
    logger.info("Completed writing data set in tfrecords format")
    
    logger.info("Saving word2index lookups")
    
    iom.save_pickle(wt_w2i,w2i_path.format("wt"))
    
    iom.save_pickle(ct_w2i,w2i_path.format("ct"))
    
    logger.info("Create base configuration files")
    
    conf_templ.create_config_template(path = iom.join_paths([config_dir,"wt_base.json"]),
                                      task = "wt",
                                      in_dir = iom.join_paths([out_dir,"wt"]),
                                      split2n_examples = split2n_examples)
    
    conf_templ.create_config_template(path = iom.join_paths([config_dir,"ct_base.json"]),
                                      task = "ct",
                                      in_dir = iom.join_paths([out_dir,"ct"]),
                                      split2n_examples = split2n_examples)
    
        
        
  elif args.task == "nli": 
    
    logger.info("Start converting parsed MultiNLI+SNLI dataset to tfrecords")
    
    iom.make_dir(out_dir)
    
    in_txt = iom.join_paths([in_dir,"{}.txt.gz"])
    out_tf = iom.join_paths([out_dir,"{}.tfrecords"])
    freqs = iom.join_paths([in_dir,"freqs.pkl"])
    w2i_path = iom.join_paths([out_dir,"w2i.pkl"])
    w2i = get_w2i(iom.load_pickle(freqs),max_freq)
    
    tfrw = TFRecordWriter()
    
    split2n_examples = {}
    
    for split in ["train","dev"]:
      
      n_ex = tfrw.write_labeled_split(in_file = in_txt.format(split),
                                      out_file = out_tf.format(split),
                                      w2i = w2i)
      
      split2n_examples["{}".format(split)] = n_ex
      
    logger.info("Completed writing data set in tfrecords format")
    
    conf_templ.create_config_template(path = iom.join_paths([config_dir,"allnli_base.json"]),
                                      task = "nli",
                                      in_dir = out_dir,
                                      split2n_examples = split2n_examples)
    
    logger.info("Saving word2index lookups")
    
    iom.save_pickle(w2i,w2i_path)
    