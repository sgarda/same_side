#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 10:35:58 2019

@author: Samuele Garda
"""

import logging
import json 
import argparse
from collections import Counter
import smart_open 
from gensim.utils import to_utf8,simple_preprocess
from gensim.parsing import preprocessing as pp
from io_utils import IOManager as iom

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')

LABEL2INT = {"contradiction" : 0, "entailment" : 1,"neutral" : 2}


FILTERS = [lambda x : x.lower(), pp.strip_tags, pp.strip_punctuation, 
           pp.strip_multiple_whitespaces, pp.remove_stopwords,
           pp.strip_numeric, pp.strip_short]


def parse_args():
  
  parser = argparse.ArgumentParser(description='Create pre-tokenized train and test split')
  parser.add_argument('-m','--multinli', required=True, type = str,  help='Path multinli folder containing JSONLINEs')
  parser.add_argument('-s','--snli', required=True, type = str,  help='Path snli folder containing JSONLINEs')
  parser.add_argument('-o','--out', required=True, type = str, help='Dir where to store parsed data')
  
  return parser.parse_args()


def update_freq(arg_words,freqs = None):
  
  if freqs is not None:
    
    for word in arg_words:
      
      freqs[word] += 1
  

def get_instance_byte_update_word_freq(row,freqs = None):
  
  arg1_words = simple_preprocess(row["sentence1"]) # pp.preprocess_string(row["sentence1"], filters = FILTERS)
  arg2_words = simple_preprocess(row["sentence2"]) # pp.preprocess_string(row["sentence2"], filters = FILTERS)
  
  update_freq(arg1_words,freqs)
  update_freq(arg2_words,freqs)
  
  arg1 = ' '.join(arg1_words)
  arg2 = ' '.join(arg2_words)
    
  label = LABEL2INT.get(row["gold_label"],None)

  line = to_utf8("{}\t{}\t{}\n".format(arg1,arg2,label)) if label is not None else None
  
  return line


def write_split_cmpr(infiles,out_dir,out_filename,freqs = None):
  
  out_path =  iom.join_paths([out_dir,out_filename])
  
  counts = 0
  
  with smart_open.open(out_path,'wb') as outfile:
    for file in infiles:
      logger.info("Processing : {}".format(file))
      with open(file) as infile:
        for line in infile:
          j_content = json.loads(line)
          out_line = get_instance_byte_update_word_freq(j_content,freqs = freqs)
          
          if out_line is not None:
            outfile.write(out_line)
          
            counts += 1
          
          if (counts%100000)==0:
            logger.info("Parsed {} instances".format(counts))
      counts = 0
      logger.info("Completed processing.")
            
  
  
  
if __name__ == "__main__":
  
  
  args = parse_args()
  
  multinli = args.multinli
  snli = args.snli
  out_dir = args.out
  
  logger.info("Start parsing transfer corpus")
  
  iom.make_dir(out_dir)
  
  freqs = Counter()
  freqs_path = iom.join_paths([out_dir,"freqs.pkl"])
  
  train_snli = iom.join_paths([snli,"snli_1.0_train.jsonl"])
  train_multinli = iom.join_paths([multinli,"multinli_1.0_train.jsonl"])
  dev_snli = iom.join_paths([snli,"snli_1.0_dev.jsonl"])
  
#  dev_snli = iom.join_paths([snli,"snli_1.0_dev.jsonl"])
  
  
  write_split_cmpr(infiles = [train_multinli,train_snli],
                   out_dir = out_dir,
                   out_filename = 'train.txt.gz',
                   freqs = freqs)
    
  iom.save_pickle(freqs,freqs_path)
  logger.info("Saved vocabulary frequencies of size at : `{}` of size {}".format(freqs_path,len(freqs)))
      
  write_split_cmpr(infiles = [dev_snli],
                   out_dir = out_dir,
                   out_filename = 'dev.txt.gz')
    
    
    
    
    
    
  
  