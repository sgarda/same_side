#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 11:30:14 2019

@author: Samuele Garda
"""

import argparse
import logging
import numpy as np
from gensim.models import KeyedVectors
from gensim.models.fasttext import FastText
from gensim.scripts.glove2word2vec import glove2word2vec
from io_utils import IOManager as iom

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')


def parse_args():
  
  parser = argparse.ArgumentParser(description='Create embeddings matrices')
  parser.add_argument('-p','--path', required=True, type = str, help='Path to embeddings file')
  parser.add_argument('-m','--model', choices = ("glove","fasttext"),required=True, type = str, help='Type of embeddings')
  parser.add_argument('-n','--out-nli', type = str, help='Folder to ALL NLI TFRECORDS')
  parser.add_argument('-s','--out-same-side', type = str, help='Folder to SAME SIDE TFRECORDS')
  
  return parser.parse_args()
  
def load_glove(path):
  
  we_w2v_format_path = iom.join_paths([iom.folder_name(path),"tmp_w2v.txt"])
  
  if not iom.check_exists(we_w2v_format_path):
    
    glove2word2vec(path, we_w2v_format_path)
  
  model = KeyedVectors.load_word2vec_format(we_w2v_format_path)
    
  return model

def load_fasttext(path):
  
  model =  FastText.load_fasttext_format(path)
  
  model.init_sims(replace = True)
  
  return model


def create_glove_embeddings(path):
  
  we_w2v_format_path = iom.join_paths([iom.folder_name(path),"tmp_w2v.txt"])
  
  if not iom.check_exists(we_w2v_format_path):
    
    glove2word2vec(path, we_w2v_format_path)
    
  model = KeyedVectors.load_word2vec_format(we_w2v_format_path)
  
  model.init_sims(replace = True)
  
  return model
    
    
def create_embedding_matrix(model,vocab,out_file):
  
  not_found = 0
  
  embedding_matrix = np.zeros((len(vocab)+1,model.vector_size))
  
  for word,idx in vocab.items():
    try:
      embedding_matrix[idx] = model[word]
    except KeyError:
      not_found += 1
  
  np.save(out_file,embedding_matrix)
  logger.info("Saved embedding matrix at `{}`".format(out_file))
  if not_found:
    logger.info("No vectors for {} words".format(not_found))
  
  del model
  
  
if __name__ == "__main__":
  
  args = parse_args()
  
  embd_path = args.path
  embd_type = args.model
  out_nli = args.out_nli
  out_same_side = args.out_same_side
  
  model = load_glove(embd_path) if embd_type == "glove" else load_fasttext(embd_path)
    
  same_side_w2i_path = iom.join_paths([out_same_side,"{}","w2i.pkl"])
  
  create_embedding_matrix(model = model,
                            vocab = iom.load_pickle(same_side_w2i_path.format("wt")),
                            out_file = iom.join_paths([out_same_side,"wt","{}_embd.npy".format(embd_type)]))
    
  create_embedding_matrix(model = model,
                          vocab = iom.load_pickle(same_side_w2i_path.format("ct")),
                          out_file = iom.join_paths([out_same_side,"ct","{}_embd.npy".format(embd_type)]))
      
  nli_w2i_path = iom.join_paths([out_nli,"w2i.pkl"])
    
  create_embedding_matrix(model = model,
                          vocab = iom.load_pickle(nli_w2i_path),
                          out_file = iom.join_paths([out_nli,"{}_embd.npy".format(embd_type)]))
    
  
  
  
  
  