#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 10:05:35 2019

@author: Samuele Garda
"""

import logging
import argparse
import json
import numpy as np
import tensorflow as tf
from io_utils import IOManager as iom
from tfrecord_utils import TFRecordParser
from models import create_model
from scripts.parse_same_side import get_reader

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')

def parse_args():
  
  parser = argparse.ArgumentParser(description='Run experiments on preprocessed Same Side data set')
  parser.add_argument('-d','--dir', required=True, type = str, help='Path dir containing original files')
  parser.add_argument('-c','--config', required=True, type = str, help='Path to configuration file')
  parser.add_argument('-w','--weights', required=True, type = str, help='Path to model weights')
  parser.add_argument('-o','--out', required=True, type = str, help='Path where to store predictions')
  
  return parser.parse_args()



FULL_NAME = {"wt" : "within-topic",
             "ct" : "cross-topic"}

if __name__ == "__main__":
  
  tf.compat.v1.enable_eager_execution()
  
  args = parse_args()
  orig_dir = args.dir
  config_path = args.config
  exp_ref_file = iom.base_name(config_path.replace(".json",".csv"))
  out_dir = args.out
  model_checkpoint = args.weights
  
  iom.make_dir(out_dir)
  out_file = iom.join_paths([out_dir,exp_ref_file])
  
  config = json.load(open(config_path))
  in_dir = config["in_dir"]
  task = config["task"]
  
  embd_weights = np.load(iom.join_paths([in_dir,config["embd"]]))
  
  tfrp = TFRecordParser()
  
  in_tf = iom.join_paths([in_dir,"{}.tfrecords"])
  in_orig = get_reader(path = iom.join_paths([orig_dir,FULL_NAME.get(task),"test.csv"]),
                       chunksize = None,
                       train = False)
  
  tid_orig = list(in_orig["id"])
        
  test_data = tfrp.create_test_tensors(path = in_tf.format("test"),
                                       batch_size = 1,
                                       max_len = config["max_len"])
    
  model = create_model(config = config,
                       embd_weights = embd_weights,
                       task = task,
                       training = False)

  model.load_weights(model_checkpoint, by_name = True)
  
  for tid,(arg1,arg2) in zip(tid_orig,test_data):
    pred = model.predict([arg1,arg2])
    print(tid,pred,sep = ' -> ')
 
  

  
  
  
  
  