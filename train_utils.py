#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 15:48:54 2019

@author: Samuele Garda
"""

import numpy as np
import warnings
from tensorflow.keras.callbacks import Callback
from tensorflow.keras import backend as K

class StopTrainingLRThreshold(Callback):
  
  def __init__(self,lr_threshold,verbose = 0):
    
    super(StopTrainingLRThreshold,self).__init__()
    
    self.lr_threshold = lr_threshold
    self.verbose = verbose
    
  def on_train_batch_begin(self,batch, logs = None):
    pass
  
  def on_train_batch_end(self, batch, logs=None):
    pass
  
  def on_test_batch_begin(self,batch, logs = None):
    pass
  
  def on_test_batch_end(self, batch, logs=None):
    pass

  def on_train_begin(self, logs=None):
    
    if not hasattr(self.model.optimizer, 'lr'):
      raise ValueError('Optimizer must have a "lr" attribute.')
  
  def on_train_end(self,logs = None):
    pass
  
  def on_test_begin(self, logs=None):
    
    if not hasattr(self.model.optimizer, 'lr'):
      raise ValueError('Optimizer must have a "lr" attribute.')
      
  def on_test_end(self, logs = None):
    pass
      
  def on_epoch_begin(self,epoch,logs=None):
    pass
  
  def on_epoch_end(self,epoch,logs = None):
    
    logs = logs or {}
    logs['lr'] = K.get_value(self.model.optimizer.lr)
    curr_lr = logs.get("lr")
    
    if curr_lr < self.lr_threshold:
      if self.verbose > 0:
        print("Epoch {} - Stop training - lr : {}".format(epoch,curr_lr))
      
      self.model.stop_training = True


class ReduceLrOnDevAccDec(Callback):
  
  def __init__(self,factor):
    super(ReduceLrOnDevAccDec,self).__init__()
    self.prev_dev_acc = -np.Inf
    self.factor = factor
  
  def on_epoch_end(self,epoch,logs = None):
    
    logs = logs or {}
    logs['lr'] = K.get_value(self.model.optimizer.lr)
    curr_lr = logs.get("lr")
    curr_dev_acc = logs.get("val_acc")
    prev_dev_acc = self.prev_dev_acc
    
    if curr_dev_acc is None:
      warnings.warn("Reduce LR on plateau conditioned on metric `val_acc`which is not available. Available metrics are: {}".format(','.join(list(logs.keys()))), RuntimeWarning)
    
    if curr_dev_acc < prev_dev_acc:
      new_lr = curr_lr * self.factor
      K.set_value(self.model.optimizer.lr, new_lr)
      print("Epoch {} - Dev accuracy decreased from {} to {} - Set lr to {}".format(epoch,prev_dev_acc,curr_dev_acc,new_lr))
    
    self.prev_dev_acc = curr_dev_acc
      
    
class LogBaseMetrics(Callback):
  
  def __init__(self):
    
    super(LogBaseMetrics,self).__init__()
    
  def on_train_batch_begin(self,batch, logs = None):
    pass
  
  def on_train_batch_end(self, batch, logs=None):
    pass
  
  def on_test_batch_begin(self,batch, logs = None):
    pass
  
  def on_test_batch_end(self, batch, logs=None):
    pass
  
  def on_train_begin(self, logs=None):
    pass
    
  def on_train_end(self,logs = None):
   pass
  
  def on_test_begin(self, logs=None):
    pass
  
  def on_test_end(self, logs = None):
    pass
#    logs = logs or {}
#    loss = logs.get('val_loss')
#    acc = logs.get('val_acc')
#    print("VAL :  LOSS : {:7.5f} - ACCURACY : {:7.5f}".format(loss,acc))
  
  def on_epoch_begin(self,epoch,logs=None):
    pass
  
  def on_epoch_end(self,epoch,logs = None):
    print("EPOCH {} :  LOSS : {:7.5f} - ACCURACY : {:7.5f}".format(epoch,logs["loss"],logs["acc"]))
  

class NBatchLogger(Callback):
    """
    A Logger that log average performance per `display` steps.
    """
    def __init__(self, display,tot_steps):
        self.step = 0
        self.tot_steps = tot_steps
        self.display = display
        self.metric_cache = {}

    def on_batch_end(self, batch, logs={}):
        self.step += 1
        for k in self.params['metrics']:
            if k in logs:
                self.metric_cache[k] = self.metric_cache.get(k, 0) + logs[k]
        if self.step % self.display == 0:
            metrics_log = ''
            for (k, v) in self.metric_cache.items():
                val = v / self.display
                if abs(val) > 1e-3:
                    metrics_log += ' - %s: %.4f' % (k, val)
                else:
                    metrics_log += ' - %s: %.4e' % (k, val)
            print('step: {}/{} ... {}'.format(self.step,
                                          self.tot_steps,
                                          metrics_log))
            self.metric_cache.clear()
    
#  

#
#class NBatchLogger(Callback):
#    def __init__(self,display=100):
#        '''
#        display: Number of batches to wait before outputting loss
#        '''
#        self.seen = 0
#        self.display = display
#
#    def on_batch_end(self,batch,logs={}):
#        self.seen += logs.get('size', 0)
#        if self.seen % self.display == 0:
#            print('\n{0}/{1} - Batch Loss: {2}'.format(self.seen,self.params['nb_sample'],
#                                                self.params['metrics'][0]))
#
#  