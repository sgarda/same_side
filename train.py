#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 17:31:41 2019

@author: Samuele Garda
"""

import logging
import argparse
import json
import numpy as np
import tensorflow as tf
from tensorflow.keras import backend as K
from models import create_model
from tensorflow.keras.optimizers import SGD,Adam
from tensorflow.keras.callbacks import EarlyStopping,ModelCheckpoint,ReduceLROnPlateau
from train_utils import StopTrainingLRThreshold
from io_utils import IOManager as iom
from io_utils import ResultWriter 
from tfrecord_utils import TFRecordParser

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')


def parse_args():
  
  parser = argparse.ArgumentParser(description='Run experiments on preprocessed Same Side data set')
  parser.add_argument('-c','--config', required=True, type = str, help='Path to configuration file')
  parser.add_argument('-r','--result', required=True, type = str, help='Dir where results are stored')
  parser.add_argument('-m','--models', required=True, type = str, help='Dir where models are stored')
  parser.add_argument('-j','--jobs', default = 4, type = int, help='Limit to this amount of cores')

  return parser.parse_args()



if __name__ == "__main__":
  
#  tf.compat.v1.enable_eager_execution()
  
  args = parse_args()
  
  config = tf.ConfigProto(intra_op_parallelism_threads=args.jobs, 
                        inter_op_parallelism_threads=args.jobs, 
                        allow_soft_placement=True, 
                        device_count = {'CPU': args.jobs})
  
  session = tf.Session(config=config)
  
  K.set_session(session)
  config_path = args.config
  res_dir = args.result
  exp_ref_name = iom.base_name(config_path.replace(".json",""))
  
  models_dir = iom.join_paths([args.models,exp_ref_name])
  
  iom.make_dir(res_dir)
  iom.make_dir(models_dir)
  
  config = json.load(open(config_path))
  in_dir = config["in_dir"]
  task = config["task"]
  
  embd_weights = np.load(iom.join_paths([in_dir,config["embd"]]))
  
  checkpoint_cb = ModelCheckpoint(filepath = iom.join_paths([models_dir,"model.{epoch:02d}.h5"]),
                                  monitor = "val_acc",
                                  save_weights_only = True,
                                  save_best_only = True,
                                  mode = "max")
  
  tfrp = TFRecordParser()
  
  in_tf = iom.join_paths([in_dir,"{}.tfrecords"])
        
  train_data = tfrp.create_labeled_tensors(path = in_tf.format("train"),
                                           batch_size = config["batch_size"],
                                           epochs = config["epochs"],
                                           max_len = config["max_len"],
                                           train = True)
    
  val_data = tfrp.create_labeled_tensors(path = in_tf.format("dev"),
                                         epochs = config["epochs"],
                                         batch_size = config["batch_size"],
                                         max_len = config["max_len"],
                                         train = False)
  
  if task == "nli":
    
    logger.info("TRAINING MODEL ON NLI TASK")
    
    res_writer = ResultWriter(path = iom.join_paths([res_dir,"allnli.csv"]))
        
    model = create_model(config = config,
                         embd_weights = embd_weights,
                         task = task,
                         training = True)
        
    opt = SGD(lr = 0.1, clipnorm = 1.)
        
    model.compile(optimizer = opt,
                  loss = 'sparse_categorical_crossentropy',
                  metrics = ['accuracy'])
    
    lr_scheduler = ReduceLROnPlateau(monitor = "val_acc",
                                     factor = 1/5,
                                     patience = 1,
                                     mode = "max",
                                     min_delta = 0.01,
                                     min_lr = 0.00001)
        
    
    train_scheduler = StopTrainingLRThreshold(lr_threshold = 0.0001, verbose = 1 )
    
        
    history = model.fit(x = train_data,
                        epochs = config["epochs"],
                        steps_per_epoch = config["n_train"] // config["batch_size"],
                        callbacks = [lr_scheduler,train_scheduler,checkpoint_cb],
                        validation_data = val_data,
                        validation_steps = config["n_dev"] // config["batch_size"],
                        verbose = 2) # CHANGE BEFORE COMMIT TO `2`
    
    
    res_writer.add_result(config_name = exp_ref_name,
                          config_dict = config,
                          dev_acc = history.history.get("val_acc")[-1])
    
  elif task == 'wt' or task == 'ct':
    
    logger.info("TRAINING MODEL ON SAME SIDE TASK : {}".format(task))
    
    res_writer = ResultWriter(path = iom.join_paths([res_dir,"{}.csv".format(task)]))
    
    model = create_model(config = config,
                         embd_weights = embd_weights,
                         task = task,
                         training = True)
    
    if config["pt_weights"] is not None:
      
      logger.info("LOADING MODEL WEIGHTS TRAINED ON NLI TASK FROM  : {}".format(config["pt_weights"]))
      
      model.load_weights(config["allnli_weights"], by_name = True)
    
    
    opt = Adam(clipnorm = 1.)
    model.compile(optimizer = opt,
                  loss = 'binary_crossentropy',
                  metrics = ['accuracy'])
   
    early_stopping = EarlyStopping(monitor='val_loss', min_delta=0,
                                   patience=3,mode = 'min',verbose = 1)
    
    history = model.fit(x = train_data,
                        epochs = config["epochs"],
                        steps_per_epoch = config["n_train"] // config["batch_size"],
                        callbacks = [early_stopping,checkpoint_cb],
                        validation_data = val_data,
                        validation_steps = config["n_dev"] // config["batch_size"],
                        verbose = 2
                        )
    
    res_writer.add_result(config_name = exp_ref_name,
                          config_dict = config,
                          dev_acc = history.history.get("val_acc")[-1])
    
    



