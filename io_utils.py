#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 16:02:43 2019

@author: Samuele Garda
"""

import glob
import pickle
import logging 
import json
from collections import OrderedDict

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')



class IOManager:
  
  @staticmethod
  def remove_file(path):
    
    if glob.os.path.exists(path):
      glob.os.remove(path)
      
      
  @staticmethod
  def base_name(path):
    
    return glob.os.path.basename(path)
  
  @staticmethod
  def folder_name(path):
    
    return glob.os.path.dirname(path)
    
  @staticmethod
  def check_exists(path):
    
    return glob.os.path.exists(path)
  
  @staticmethod
  def make_dir(path):
    
    if not glob.os.path.exists(path):
      glob.os.makedirs(path)
      
  @staticmethod
  def join_paths(paths):
    return glob.os.path.join(*paths)
  
  @staticmethod
  def save_pickle(item,path):
    
    if not glob.os.path.exists(path):
  
      pickle.dump( item, open( str(path), 'wb'))
  
    else:
  
      raise ValueError("File {} already exists! Not overwriting".format(path))
      
  @staticmethod
  def load_pickle(path):
    
    if glob.os.path.exists(path):
    
      item = pickle.load(open(str(path), mode = "rb"))
  
    else:
      
      raise ValueError("File {} not found!".format(path))
      
    return item
  


class ConfigTemplate(object):
  
  def __init__(self):
  
    
    self.base_tamplete = OrderedDict([("task",None),
                                     ("in_dir",None),
                                     ("gpu",False),
                                     ("max_len",200), 
                                     ("embd","glove_embd.npy"),
                                     ("n_train",None),
                                     ("n_dev",None),
                                     ("n_test",None),
                                     ("train_enc",True),
                                     ("use_bn_mlp",True),
                                     ("sent_vec_extr","average"),
                                     ("ngram_cross_att",5),
                                     ("enc_type","lstm"),
                                     ("enc_size",1024),
                                     ("comb_mode","cross_att"),
                                     ("mlp_size",1024),
                                     ("att_size", 256),
                                     ("bn_mlp_size",256),
                                     ("embd_dropot",0.0),
                                     ("enc_dropout",0.0),
                                     ("mlp_dropout",0.2),
                                     ("rec_dropout",0.0),
                                     ("loss",None),
                                     ("n_class",None),
                                     ("batch_size",64),
                                     ("epochs",20)]) 

  
  
  def get_config_template(self,task,in_dir,split2n_examples):
    
    if task == "nli":
      template = self.base_tamplete
      
      template["task"] = "nli"
      template["in_dir"] = in_dir
      template["n_class"] = 3
      template["loss"] = "sparse_categorical_crossentropy"
      template["batch_size"] = 64
      template["n_train"] = split2n_examples.get("train")
      template["n_dev"] = split2n_examples.get("dev")
      
      return template
  
    elif task == "wt":
      template = self.base_tamplete
      
      template["task"] = "wt"
      template["in_dir"] = in_dir
      template["n_class"] = 1
      template["loss"] = "binary_crossentropy"
      template["n_train"] = split2n_examples.get("wt_train")
      template["n_dev"] = split2n_examples.get("wt_dev")
      template["n_test"] = split2n_examples.get("wt_test")
      template["pt_weights"] = None
      
      return template
  
    elif task == "ct":
      
      template = self.base_tamplete
      
      template["task"] = "ct"
      template["in_dir"] = in_dir
      template["n_class"] = 1
      template["loss"] = "binary_crossentropy"
      template["n_train"] = split2n_examples.get("ct_train")
      template["n_dev"] = split2n_examples.get("ct_dev")
      template["n_test"] = split2n_examples.get("ct_test")
      template["pt_weights"] = None
      
      return template
      
  def create_config_template(self,path,task,in_dir,split2n_examples):
    
    template = self.get_config_template(task,in_dir,split2n_examples)
    
    with open(path,"w") as out_file:
      json.dump(template,out_file,indent = 1)
  
    
    

class ResultWriter(object):
  
  def __init__(self,path):
    
    self.path = path
    self.hps = sorted(["embd","enc_size","enc_type","comb_mode","use_bn_mlp","mlp_size","bn_mlp_size","embd_dropot",
               "enc_dropout","mlp_dropout","ngram_cross_att","sent_vec_extr","train_enc","epochs","batch_size"])
    self._write_header()
    
  
  def _write_header(self):
    
    if not glob.os.path.exists(self.path):
      with open(self.path,"w") as out_file:
        line = "config,{},dev_acc\n".format(','.join(self.hps))
        out_file.write(line)
  
  def add_result(self,config_name,config_dict,dev_acc):
    
    conditions = ','.join([str(config_dict.get(hp)) for hp in self.hps])
    
    with open(self.path, 'a') as out_file:
      line = "{},{},{}\n".format(config_name,conditions,dev_acc)
      out_file.write(line)
      
      
      
      
      
#      def h5py_dataset_iterator(g, prefix=''):
#  for key in g.keys():
#    item = g[key]
#    path = '{}/{}'.format(prefix,key)
#    if isinstance(item, h5py.Dataset): 
#      yield (path, item)
#    elif isinstance(item, h5py.Group):
#      yield from h5py_dataset_iterator(item, path)
#      
#
#if __name__ == "__main__":
#  path = sys.argv[1]
#
#  with h5py.File(path,'r') as f:
#    for path,value in h5py_dataset_iterator(f):
#      print(path)