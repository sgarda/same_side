#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 18:08:00 2019

@author: Samuele Garda
"""

import tensorflow as tf


class AbsDiff(tf.keras.layers.Subtract):
  
  def build(self, input_shape):
      super(AbsDiff, self).build(input_shape)
      if len(input_shape) != 2:
          raise ValueError('An `AbsDiff` layer should be called ''on exactly 2 inputs')
  
  def _merge_function(self, inputs):
    if len(inputs) != 2:
      raise ValueError('An `AbsDiff` layer should be called on exactly 2 inputs')
    
    return tf.math.abs(inputs[0] - inputs[1])



class GLU(tf.keras.layers.Layer):

   def __init__(self,kernel_size,filters,index,trainable):
      super(GLU,self).__init__()
      self.kernel_size = kernel_size
      self.filters = filters
      self.conv = tf.keras.layers.Conv1D(kernel_size = self.kernel_size, filters = self.filters,
                                         name = "glu_conv_{}".format(index),
                                         trainable = trainable)
      self.gate = tf.keras.layers.Conv1D(kernel_size = self.kernel_size, filters = self.filters, activation = "sigmoid",
                                         name = "glu_gate_{}".format(index),
                                         trainable = trainable)
      self.padding =  tf.keras.layers.ZeroPadding1D(padding = (self.kernel_size -1,0))
      self.mult = tf.keras.layers.Multiply()

   def call(self,x):
      pad_x = self.padding(x)
      out =  self.mult([self.conv(pad_x),self.gate(pad_x)])
      return out


class GLUEncoder(tf.keras.layers.Layer):
  
  def __init__(self,enc_size,dropout,trainable = True,*args,**kwargs):
    super(GLUEncoder,self).__init__(*args,**kwargs)
    self.enc_size = enc_size
    self.ante = tf.keras.layers.Conv1D(kernel_size = 5,
                                       filters = self.enc_size, 
                                       padding = "same",
                                       name = "ante",
                                       trainable = trainable)
    
    self.glu_block1 = GLUBlock(enc_size = enc_size,dropout = dropout,trainable = trainable) 
  
  def call(self,x):
    
    res = self.ante(x)
    out = self.glu_block1(res)

    return out
    
    
    

    
class GLUBlock(tf.keras.layers.Layer):
  
  def __init__(self,enc_size,dropout,trainable = True,*args,**kwargs):
    super(GLUBlock,self).__init__(*args,**kwargs)
    self.enc_size = enc_size
  
    self.glus = [GLU(kernel_size = 1,filters = self.enc_size // 2, index = 0,trainable = trainable),
                 GLU(kernel_size = 5,filters = self.enc_size // 2, index = 1,trainable = trainable)]
    
    self.post = tf.keras.layers.Conv1D(kernel_size = 1,
                                       filters = self.enc_size, 
                                       padding = "same",
                                       name = "post",
                                       trainable = trainable)
    
    self.do = tf.keras.layers.Dropout(rate = dropout)
  
  def call(self,x):    
    pre_out = x
    for glu in self.glus:
      pre_out = glu(pre_out)
      
    out = self.do(self.post(pre_out)) + x
    
    return out
  
class BaseCombine(tf.keras.layers.Layer):
  
  def __init__(self):
    super(BaseCombine,self).__init__()
    self.concat = tf.keras.layers.Concatenate()
  
  def call(self,inputs,mask = None):
    
    arg1,arg2 = inputs
    
    xy = self.concat([arg1,arg2])
    m = arg1 * arg2
    ad = tf.abs(arg1-arg2)
    
    return self.concat([xy,m,ad])



class NonZeroAverage(tf.keras.layers.GlobalAveragePooling1D):
  
  def __init__(self):
    super(NonZeroAverage,self).__init__()
    self.support_masking = True
    
  def call(self,x,mask = None,length=None):
    
    sums = tf.reduce_sum(x,axis = 1)
    avg = tf.math.divide(sums, tf.expand_dims(length, -1))

    return avg
    
    
class GlobalMaxPooling1DWithMasking(tf.keras.layers.GlobalAveragePooling1D):
  
  def __init__(self):
    super(GlobalMaxPooling1DWithMasking,self).__init__() 
    self.support_masking = True
 
  def call(self,x,mask=None,length = None):
    
    steps_axis = 1 if self.data_format == 'channels_last' else 2
    return tf.keras.backend.max(x, axis=steps_axis)
  


class CrossAttention(tf.keras.layers.GlobalAveragePooling1D):
  
  def __init__(self,ngram_size,att_size):
    super(CrossAttention,self).__init__()
    self.att_encout = tf.keras.layers.Conv1D(kernel_size = ngram_size,
                                           filters = att_size,
                                           padding = "same",
                                           )
    self.att_svec = tf.keras.layers.Dense(units=att_size)
    self.mult = tf.keras.layers.Multiply()
    
  def call(self,enc_out,svec,mask):
    
    encout_att = self.att_encout(enc_out)
    svec_att = self.att_svec(svec)
    svec_att = tf.expand_dims(svec_att,axis = 1)
    comp = tf.matmul(encout_att,svec_att,transpose_b = True)
    comp += tf.expand_dims((mask * -1e9),axis= -1)
    att_weights = tf.nn.softmax(comp, axis = 1)
    weigheted_out = enc_out * att_weights
    out = tf.reduce_sum(weigheted_out, axis = 1)
    
    return out
  
if __name__ == "__main__":


  import numpy as np
  
  x = np.random.rand(2,32,100)
  
  glu_block = GLUBlock(enc_size = 512, dropout = 0.1)
  
  res = glu_block(x)

#  glu1 = GLU(kernel_size = 1, filters = 128)
#  glu2 = GLU(kernel_size = 5, filters = 128)
#
#  pre_out = glu1(x)
#  res = glu2(pre_out)

  
#  glu_block = GLUBlock(enc_size = 512,dropout = 0.1)
#  
#  x = np.random.rand(2,32,100)
#  
#  res = glu_block(x)
#  
#  print(res.shape)
  
#  source = (
#      ((tf.constant(np.random.normal(0, 1, (1024, 2)), dtype=tf.float32),
#        tf.constant(np.random.normal(0, 1, (1024, 2)), dtype=tf.float32))),
#      tf.constant(np.random.randint(0, 2, (1024, 2)), dtype=tf.float32)
#  )
#  
#  train = tf.data.Dataset.from_tensor_slices(source).batch(128, drop_remainder=True).repeat(10)
#  valid = tf.data.Dataset.from_tensor_slices(source).batch(128, drop_remainder=True).repeat(10)
#  
#  
#  class Model(tf.keras.Model):
#      def __init__(self):
#          super(Model, self).__init__()
#          self.d = tf.keras.layers.Dense(2, activation="softmax")
#  
#      def call(self, inputs, training=True, mask=None):
#          return self.d(inputs[0] + inputs[1])
#  
#  
#  m = Model()
#  m.compile(optimizer = "adam",
#            loss=["categorical_crossentropy"])
#  m.fit(x=train, validation_data=valid, steps_per_epoch=8)    
#    