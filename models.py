#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 10:49:57 2019

@author: Samuele Garda
"""
import tensorflow as tf
from layers import GLUEncoder,BaseCombine,GlobalMaxPooling1DWithMasking,NonZeroAverage,CrossAttention


def create_model(config,embd_weights,training,task):
  
  arg1 = tf.keras.layers.Input(shape = (None,), name = "arg1")
  arg2 = tf.keras.layers.Input(shape = (None,), name = "arg2")
  
  mask1 = tf.cast(tf.math.equal(arg1, 0), tf.float32)
  len1 = tf.reduce_sum(tf.cast(tf.math.not_equal(arg1, 0), tf.float32),axis = 1)
  
  mask2 = tf.cast(tf.math.equal(arg2, 0), tf.float32)
  len2 = tf.reduce_sum(tf.cast(tf.math.not_equal(arg2, 0), tf.float32),axis = 1)
  
  embd = tf.keras.layers.Embedding(input_dim = embd_weights.shape[0],
                                   output_dim = embd_weights.shape[1],
                                   embeddings_initializer = tf.keras.initializers.Constant(embd_weights),
                                   mask_zero = True if (config["enc_type"] == "lstm" and not config["gpu"]) else False,
                                   trainable = False if task == "nli" else True,
                                   name = "embd_{}".format(task))
  
  embd1 = embd(arg1)
  embd2 = embd(arg2)
  
  pre_mlp = Arg1Arg2Vectorizer(enc_type = config["enc_type"],
                               enc_size = config["enc_size"],
                               comb_mode = config["comb_mode"],
                               embd_do = config["embd_dropot"],
                               enc_do = config["enc_dropout"],
                               rec_do = config["rec_dropout"],
                               sent_vec_extr = config["sent_vec_extr"],
                               train_enc = config["train_enc"],
                               gpu = config["gpu"],
                               att_size = config["att_size"],
                               ngram_cross_att = config["ngram_cross_att"])(inputs = [embd1,embd2], mask1 = mask1,mask2 = mask2,len1 = len1,len2 = len2, training = training)
  
  if config["use_bn_mlp"]:
    pre_mlp = tf.keras.layers.Dropout(rate = config["mlp_dropout"])(pre_mlp,training = training)
    pre_mlp = tf.keras.layers.Dense(units = config["bn_mlp_size"],
                                    activation = "relu",
                                    name = "bn_mlp_{}".format(task))(pre_mlp)
    
  pre_out = tf.keras.layers.Dropout(rate = config["mlp_dropout"])(pre_mlp,training = training)
  pre_out = tf.keras.layers.Dense(units = config["mlp_size"],
                                  activation = "relu",
                                  name = "mlp_{}".format(task))(pre_out)
    
  logits = tf.keras.layers.Dense(units = config["n_class"],
                                 activation = "sigmoid" if config["n_class"] == 1 else "softmax",
                                 name = "logits_weights_{}".format(task))(pre_out)
  
  model = tf.keras.models.Model(inputs = [arg1,arg2], outputs = logits, name = "arg1arg2")

  return model




class Arg1Arg2Vectorizer(tf.keras.layers.Layer):
  
  def __init__(self,enc_type = "lstm",enc_size = 512,comb_mode = "base",embd_do = 0.0,enc_do = 0.0,rec_do = 0.0,gpu = False,
               sent_vec_extr = "maxpool", ngram_cross_att = 5,train_enc = False,att_size = 100):
    super(Arg1Arg2Vectorizer,self).__init__()
    self.comb_mode = comb_mode
    self.sent_vec_extr = sent_vec_extr
    
    self.sdo = tf.keras.layers.SpatialDropout1D(rate = embd_do)
    self.encoder = self.get_encoder(enc_type,enc_size,enc_do,rec_do,gpu,train_enc)
    self.sent_extractor = self.get_sent_extractor(sent_vec_extr)
    self.combine = self.get_combine(comb_mode,ngram_cross_att,att_size)
    self.base_combine = BaseCombine()
    
  def get_sent_extractor(self,sent_vec_extr):
    
    if sent_vec_extr == "maxpool":
      
      extractor = GlobalMaxPooling1DWithMasking()
    
    elif sent_vec_extr == "average":
      
      extractor = NonZeroAverage()
    
    else:
      
      raise ValueError("Extractor not available! Use either `maxpool` or `average`! Found {}".format(sent_vec_extr))
      
    return extractor
  
  
  def get_combine(self,comb_mode,ngram_cross_att,att_size):
    
    if comb_mode == "base":
      
      combine = BaseCombine()
    
    elif comb_mode == "cross_att": 
      
      combine = CrossAttention(ngram_cross_att,att_size)
      
    else:
      
      raise ValueError("Combination mode not available! Use either `base` or `cross_att`! Found {}".format(comb_mode))
    
    return combine
  
  def get_encoder(self,enc_type,enc_size,enc_do,rec_do,gpu,trainable = True):
    
    if (enc_type == "lstm" and gpu):
      
      enc = tf.keras.layers.Bidirectional(tf.keras.layers.CuDNNLSTM(units = enc_size // 2,
                                                                    return_sequences = True,
                                                                  name = "encoder",
                                                                  trainable = trainable))
    
    elif (enc_type == "lstm" and not gpu):
      
      enc = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units = enc_size // 2,
                                                               return_sequences = True,
                                                             dropout = enc_do,
                                                             recurrent_dropout = rec_do,
                                                             name = "encoder",
                                                             trainable = trainable))
    
    elif enc_type == "glu": 
      
      enc = GLUEncoder(enc_size = enc_size, dropout = enc_do,trainable = trainable, name = "encoder")
      
    
    else:
      
      raise ValueError("Encoder not available! Use either `lstm` or `glu`! Found {}".format(enc_type))
    
    return enc
  
  def call(self,inputs,mask1,mask2 ,len1,len2,training):
    
    embd1 = self.sdo(inputs[0],training = training)
    embd2 = self.sdo(inputs[1],training = training)
        
    enc1 = self.encoder(embd1)
    enc2 = self.encoder(embd2)
        
    svec1 = self.sent_extractor(enc1,length = len2,mask = None)
            
    svec2 = self.sent_extractor(enc2,length = len2,mask = None)
        
    if self.comb_mode == "base":
      out = self.combine([svec1,svec2])
    else:
      out1 = self.combine(enc1,svec = svec2,mask = mask1)
      out2 = self.combine(enc2,svec = svec1,mask = mask2)
      out = self.base_combine([out1,out2])
    
    return out
