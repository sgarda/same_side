#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 17:50:55 2019

@author: Samuele Garda
"""

import logging
import smart_open
import tensorflow as tf

logger = logging.getLogger(__name__)
logging.basicConfig(format = '%(asctime)s : %(levelname)s : %(module)s: %(message)s', level = 'INFO')


class TFRecordWriter(object):
  
  def text2ids(self,txt_string,vocab):
  
    return [vocab.get("<S>")] + [vocab.get(w) for w in txt_string.split() if w in vocab]
  
  
  def _int64_feature(self,value):
    """
    Map list of ints to tf compatible Features
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))
  
  
  def write_labeled_example(self,writer,arg1,arg2,label,w2i):
    
    array_arg1 = self.text2ids(arg1,w2i)
    length_arg1 = [len(array_arg1)]
    array_arg2 = self.text2ids(arg2,w2i)
    length_arg2 = [len(array_arg2)]
    array_label = [int(label)]

    
    features = {"arg1" : self._int64_feature(array_arg1),
                "length1" : self._int64_feature(length_arg1),
                "arg2" : self._int64_feature(array_arg2),
                "length2" : self._int64_feature(length_arg2), 
                "label" : self._int64_feature(array_label)}
    
    example = tf.train.Example(features=tf.train.Features(feature=features))
    
    writer.write(example.SerializeToString())
    
  
  def write_test_exmaple(self,writer,arg1,arg2,w2i):
    
    array_arg1 = self.text2ids(arg1,w2i)
    length_arg1 = [len(array_arg1)]
    array_arg2 = self.text2ids(arg2,w2i)
    length_arg2 = [len(array_arg2)]

    
    features = {"arg1" : self._int64_feature(array_arg1),
                "length1" : self._int64_feature(length_arg1),
                "arg2" : self._int64_feature(array_arg2),
                "length2" : self._int64_feature(length_arg2)}
    
    example = tf.train.Example(features=tf.train.Features(feature=features))
    
    writer.write(example.SerializeToString())
    
    
  def write_labeled_split(self,in_file,out_file,w2i):
    
    writer = tf.io.TFRecordWriter(out_file)
    
    n_examples = 0
    
    with smart_open.open(in_file) as infile:
      
      for idx,line in enumerate(infile):
        
        arg1,arg2,label = line.strip("\n").split("\t")
        
        self.write_labeled_example(writer,arg1,arg2,label,w2i)
        
        n_examples += 1
        
        if (idx%10000)==0:
          
          logger.info("Written {} examples to {}".format(idx,out_file))
    
    return n_examples
  
  def write_test_split(self,in_file,out_file,w2i):
    
    writer = tf.io.TFRecordWriter(out_file)
    
    n_examples = 0
    
    with smart_open.open(in_file) as infile:
      
      for idx,line in enumerate(infile):
        
        arg1,arg2 =  line.strip("\n").split("\t")
        
        self.write_test_exmaple(writer,arg1,arg2,w2i)
        
        n_examples += 1
        
        if (idx%10000)==0:
          
          logger.info("Written {} examples to {}".format(idx,out_file))
    
    return n_examples
    
    
class TFRecordParser(object):
      
  def sparsefeature2dense(self,sparse_item,shape):
    
    shape = tf.cast(shape,tf.int32)
    
    item = tf.reshape(tf.sparse.to_dense(sparse_item),shape)
    
    return item
    
  def parse_training_instance(self,proto,max_len):
    
    features = {"arg1" :  tf.io.VarLenFeature(tf.int64),
                "length1" : tf.io.FixedLenFeature((1,), tf.int64),
                "arg2" : tf.io.VarLenFeature(tf.int64),
                "length2" : tf.io.FixedLenFeature((1,), tf.int64), 
                "label" : tf.io.FixedLenFeature((1,), tf.int64)}
    
    parsed_features = tf.io.parse_single_example(proto, features)
    
    arg1 = self.sparsefeature2dense(sparse_item = parsed_features["arg1"],
                                    shape = parsed_features["length1"])[:max_len]
    
    arg2 = self.sparsefeature2dense(sparse_item = parsed_features["arg2"],
                                    shape = parsed_features["length2"])[:max_len]
    
    
    label = parsed_features["label"]
    
    return ((arg1,arg2),label)
  
  def parse_test_instance(self,proto,max_len):
    
    features = {"arg1" :  tf.io.VarLenFeature(tf.int64),
                "length1" : tf.io.FixedLenFeature((1,), tf.int64),
                "arg2" : tf.io.VarLenFeature(tf.int64),
                "length2" : tf.io.FixedLenFeature((1,), tf.int64)}
    
    parsed_features = tf.io.parse_single_example(proto, features)
    
    arg1 = self.sparsefeature2dense(sparse_item = parsed_features["arg1"],
                                    shape = parsed_features["length1"])[:max_len]
    
    
    arg2 = self.sparsefeature2dense(sparse_item = parsed_features["arg2"],
                                    shape = parsed_features["length2"])[:max_len]
    
    return ((arg1,arg2))
  
  
  
  def create_labeled_tensors(self,path,batch_size,epochs = 1,train = False, max_len = 200):
    
    dataset = tf.data.TFRecordDataset(path)
    
    dataset = dataset.map(lambda x : self.parse_training_instance(x,max_len))
    
    dataset = dataset.padded_batch(batch_size, 
                                   padded_shapes = ( (([None],[None]),[1]) ) 
                                   )
    
    if train:
      dataset = dataset.shuffle(buffer_size = 1000)
          
    dataset = dataset.repeat(epochs)
    
    dataset = dataset.prefetch(batch_size)
        
    return dataset
  
  def create_test_tensors(self,path,batch_size, max_len = 200):
    
    dataset = tf.data.TFRecordDataset(path)
    
    dataset = dataset.map(lambda x : self.parse_test_instance(x,max_len))
    
    dataset = dataset.padded_batch(batch_size, 
                                   padded_shapes = ( ([None],[None]) )
                                   )
        
    return dataset
    
    
    
    
    
    
    
    
    
    
    