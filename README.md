# Same Side Stance Classification

Code for shared task : https://sameside.webis.de/

## Data

First of all download:

- [Same Side Stance Classification corpus](https://github.com/webis-de/argmining19-same-side-classification) : place folders `cross-topic` and `within-topic` in a single folder
- [Stanford Natural Languge Inferece corpus](https://nlp.stanford.edu/projects/snli/) 
- [Multi-Genre NLI corpus](https://www.nyu.edu/projects/bowman/multinli/)
- [Glove word embeddings](https://nlp.stanford.edu/projects/glove/) : glove.840B.300d.zip

## Preprocessing

Go through data sets twice. Firt preprocress, collect vocabulary stats and save in a compressed format.

For preprocessing the natural language inference corpora (pre-training phase data): 

    python3 -m scripts.parse_nli --multinli <multigenre nli> --snli <snli> --out parsed_nli
    
For preprocessing the same side data:

    python3 -m scripts.parse_same_side --in-dir <same_side folder> --out parsed_same_side [--chunk 1000]
    

## Tfrecords

Create tfrecords file for using tensorflow `tf.data.Dataset` module. In this phase a basic configuration file for each run (nli, within-topic, cross-topic) is created.
This configuration file is placed in the `--config-dir` and contains the base models hyper-parameters and the number of examples in each spli of the dataset (needed later for training properly with `keras` and `tf.data.Dataset`).

    python3 -m scripts.data2tfrecords --in-dir <parsed_nli> --task nli --config-dir <configs folder> --out <tf_nli> [--max-freq 5000]
    
    python3 -m scripts.data2tfrecords --in-dir <parsed_same_side> --task same_side --config-dir <configs folder> --out <tf_same_side> [--max-freq 5000]

It is possible to encode the inputs keeping only the n most frequent words, however given the relatively small size of the vocabulary the original vocabulary is kept.


## Embeddings

Create the embedding matrix used to encode the words. Be sure to place the output of this script in the folder containing the tfrecords files.

    python3 -m scripts.we --path <path to glove> --model <glove> --out-nli <tf_nli> --out-same-side <tf_same_side>
    
We experimented as well with `fasttext` embeddings but the results were significantly worse.

## Train

All the configuration files (`allnli_base.json`,`wt_base.json`,`ct_base.json`) have the parameters to reproduce the best model on the task.

First train the model on the natural language inference task:

    python3 train.py --config <config_dir/allnli_base.json> --models <models_dir> --result <results_dir> --jobs <limit # of cores>

In the folder `--models` will be stored the best model according to the validation accuracy.

Now train the models on the same side task. Modify both configuration files for the parameter `pt_weights` to contain the path to the best model on the nli task , e.g. "nli_models/model.08.h5".


    python3 train.py --config <config_dir/wt_base.json> --models <models_dir> --result <result_dir> --jobs <limit # of cores>
    
In the `--result` directory a csv file will be created for each task containing the development accuracy of each model and its hyper-parameters. This is useful to quickly compare different settings.

## Predict

Creatr prediction files:

    python3 predict.py --dir <same_side folder> --config <config_dir/best_model.json> --weights <models_dir/best_model.h5> --out <pred_dir>

For the `--dir` parameter you must pass the folder containing the original non-parsed data (retrieve ids). The `--weights` needs to be the h5 format containing the best model weights.


## Requirements

- tensorflow >= 1.13
- gensim >= 3.7.3
- pandas >= 0.24.2
